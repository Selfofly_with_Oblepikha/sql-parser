%option noyywrap
%option caseless
%option yylineno

%{
    #include <stdio.h>

    #include "node.h"
    #include "parser.tab.h"
    void print_error(char*);
%}

%option noyywrap nounput noinput batch debug

alpha   [A-Za-z_0-9 \t\r]
id    [a-zA-Z][a-zA-Z_0-9]*
int   ([-+]?[0-9])+
float   [+-]?([0-9]*[.])?[0-9]+
blank [ \t\r]
word ([a-zA-Z_\*][a-zA-Z0-9_]*)
quoted_string \"(\\.|\"\"|[^"\n])*\"

%%


"select"    {return SELECT;}
"insert"    {return INSERT;}
"update"    {return UPDATE;}
"create table"    {return CREATE;}
"delete"    {return DELETE;}
"drop"      {return DROP;}
"from"	    {return FROM;}
"set"       {return SET;}
"into"      {return INTO;}
"where"     {return WHERE;}
"join"      {return JOIN;}
"values"    {return VALUES;}
"on"        {return ON;}

"and"       {yylval.logic_op = 0; return AND;}
"or"        {yylval.logic_op = 1; return OR;}

"contains"  {yylval.cmp_type = 0; return CMP;}
">"         {yylval.cmp_type = 1; return CMP;}
">="        {yylval.cmp_type = 2; return CMP;}
"<"         {yylval.cmp_type = 7; return CMP;}
"<="        {yylval.cmp_type = 6; return CMP;}
"=="        {yylval.cmp_type = 3; return CMP;}
"<>"        {yylval.cmp_type = 5; return CMP;}
"substr"    {yylval.cmp_type = 8; return CMP;}

"str"       {yylval.type = 0; return TYPE;}
"int"       {yylval.type = 1; return TYPE;}
"float"     {yylval.type = 2; return TYPE;}
"bool"      {yylval.type = 3; return TYPE;}
"true"      {yylval.boolval = 1; return BOOL;}
"false"     {yylval.boolval = 0; return BOOL;}

"("         {return OPEN_CIRCLE_BRACKET;}
")"         {return CLOSE_CIRCLE_BRACKET;}
"{"         {return OPEN_FIGURE_BRACKET;}
"}"         {return CLOSE_FIGURE_BRACKET;}
";"         {return ENDLINE;}
":"         {return COLON;}
"."         {return DOT;}
","         {return COMMA;}
"="	    {return EQ;}
"*"	    {return STAR;}

{quoted_string} { 	
	sprintf(yylval.str, "%s", yytext); 
	return (QSTR); 
}

{word}     {
    sscanf(yytext, "%s", yylval.str);
    return (STR);
}
{int}   {
    yylval.intval = atoi(yytext);
    return (INT);
}
{float}     {
    yylval.floatval = atof(yytext);
    return (FLOAT);
}

[ \t]   { /* ignore */ }
[\n]    {}
.           {
    print_error(yytext);
    return (OTHER);
}

%%

void print_error(char* token) {
    printf("Error in tokenizer token = %s \n", token);
}


