#include "utils.h"
#include <stdio.h>
#include <stdlib.h>

char* str_copy (const char* from) {
    int count = 1;
    while (from[count] != '\0') count++;
    char* ret = malloc(sizeof(char) * (count + 1));
    for (int i = 0; i < count; i++) {
        ret[i] = from[i];
    }
    ret[count] = '\0';
    return ret;
}

void str_concat(char** str, const char * str2) {
    char* str1 = *str;
    int first_len = 0, second_len = 0;
    while (str1[first_len] != '\0') {
        first_len++;
    }
    while (str2[second_len] != '\0') {
        second_len++;
    }
    char * new_str = malloc(sizeof(char) * (first_len + second_len + 1));
    for (int i = 0; i < first_len; i++) {
        new_str[i] = str1[i];
    }
    for (int i = first_len; i < first_len + second_len; i++) {
        new_str[i] = str2[i - first_len];
    }
    new_str[first_len + second_len] = '\0';
    free(str1);
    *str = new_str;
}

char* String_to_string(Node* node, int tabs) {
    char* res = malloc(sizeof(char));
    res[0] = '\0';
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "String { ");
    str_concat(&res, node->v_first.str);
    str_concat(&res, " }");
    return res;
}

char* Column_to_string(Node* node, int tabs) {
    char* res = malloc(sizeof(char));
    res[0] = '\0';
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "Column {\n");
    for (int i = 0; i < tabs + 1; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "table: ");
    str_concat(&res, node->v_first.str);
    str_concat(&res, "\n");
    for (int i = 0; i < tabs + 1; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "col_name: ");
    str_concat(&res, node->v_second.str);
    str_concat(&res, "\n");
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "}");
    return res;
}

char* Integer_to_string(Node* node, int tabs) {
    char* res = malloc(sizeof(char));
    res[0] = '\0';
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "Integer { ");
    int length = snprintf(NULL, 0, "%d", node->v_first.integer);
    char* str = malloc( length + 1 );
    snprintf(str, length + 1, "%d", node->v_first.integer);
    str_concat(&res, str);
    free(str);
    str_concat(&res, " }");
    return res;
}

char* Float_to_string(Node* node, int tabs) {
    char* res = malloc(sizeof(char));
    res[0] = '\0';
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "Float { ");
    int length = snprintf(NULL, 0, "%f", node->v_first.flt);
    char* str = malloc( length + 1 );
    snprintf(str, length + 1, "%f", node->v_first.flt);
    str_concat(&res, str);
    free(str);
    str_concat(&res, " }");
    return res;
}

char* Bool_to_string(Node* node, int tabs) {
    char* res = malloc(sizeof(char));
    res[0] = '\0';
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "Bool { ");
    str_concat(&res, node->v_first.boolean ? "TRUE" : "FALSE");
    str_concat(&res, " }");
    return res;
}

char* Type_to_string(Node* node, int tabs) {
    char* res = malloc(sizeof(char));
    res[0] = '\0';
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "Type { ");
    str_concat(&res, DataType_strings[node->v_first.data_type]);
    str_concat(&res, " }");
    return res;
}

char* List_to_string(Node* node, int tabs) {
    char* res = malloc(sizeof(char));
    res[0] = '\0';
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "List {\n");
    Node* next = node;
    char* r;
    while (next != NULL) {
        r = to_string(next->first, tabs + 1);
        str_concat(&res, r);
        free(r);
        str_concat(&res, ";\n");
        next = next->second;
    }
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "}");
    return res;
}

char* Pair_to_string(Node* node, int tabs) {
    char* res = malloc(sizeof(char));
    res[0] = '\0';
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "Pair {\n");
    for (int i = 0; i < tabs + 1; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "col_name: ");
    str_concat(&res, node->v_first.str);
    str_concat(&res, "\n");
    char* r = to_string(node->second, tabs + 1);
    str_concat(&res, r);
    free(r);
    str_concat(&res, "\n");
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "}");
    return res;
}

char* Select_to_string(Node* node, int tabs) {
    char* res = malloc(sizeof(char));
    res[0] = '\0';
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "Select {\n");
    char* r;

    for (int i = 0; i < tabs + 1; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "columns: ");
    if (node->third != NULL) {
        str_concat(&res, "\n");
        r = to_string(node->third, tabs + 2);
        str_concat(&res, r);
        free(r);
    } else {
        str_concat(&res, "*");
    }
    str_concat(&res, "\n");

    for (int i = 0; i < tabs + 1; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "table: ");
    str_concat(&res, node->v_first.str);
    str_concat(&res, "\n");

    for (int i = 0; i < tabs + 1; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "join: ");
        if (node->v_second.str != NULL) {
        str_concat(&res, node->v_second.str);
    } else {
        str_concat(&res, "NULL");
    }
    str_concat(&res, "\n");

    for (int i = 0; i < tabs + 1; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "on: ");
    if (node->second != NULL) {
        str_concat(&res, "\n");
        r = to_string(node->second, tabs + 2);
        str_concat(&res, r);
        free(r);
    } else {
        str_concat(&res, "NULL");
    }

    str_concat(&res, "\n");
    for (int i = 0; i < tabs + 1; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "condition: ");
    if (node->first != NULL) {
        str_concat(&res, "\n");
        r = to_string(node->first, tabs + 2);
        str_concat(&res, r);
        free(r);
    } else {
        str_concat(&res, "NULL");
    }
    str_concat(&res, "\n");

    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "}");
    return res;
}

char* Delete_to_string(Node* node, int tabs) {
    char* res = malloc(sizeof(char));
    res[0] = '\0';
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "Delete {\n");
    for (int i = 0; i < tabs + 1; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "table: ");
    str_concat(&res, node->v_first.str);
    str_concat(&res, "\n");
    for (int i = 0; i < tabs + 1; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "condition: ");
    char* r;
    if (node->first != NULL) {
        str_concat(&res, "\n");
        r = to_string(node->first, tabs + 2);
        str_concat(&res, r);
        free(r);
    } else {
        str_concat(&res, "NULL");
    }

    str_concat(&res, "\n");
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "}");
    return res;
}

char* Insert_to_string(Node* node, int tabs) {
    char* res = malloc(sizeof(char));
    res[0] = '\0';
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "Insert {\n");
    for (int i = 0; i < tabs + 1; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "name: ");
    str_concat(&res, node->v_first.str);
    str_concat(&res, "\n");
    char* r = to_string(node->first, tabs + 1);
    str_concat(&res, r);
    free(r);
    str_concat(&res, "\n");
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "}");
    return res;
}

char* Update_to_string(Node* node, int tabs) {
    char* res = malloc(sizeof(char));
    res[0] = '\0';
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "Update {\n");
    for (int i = 0; i < tabs + 1; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "table: ");
    str_concat(&res, node->v_first.str);
    str_concat(&res, "\n");
    for (int i = 0; i < tabs + 1; i++) {
        str_concat(&res, "   ");
    }
    char* r;
    str_concat(&res, "list_values:");
    if (node->second != NULL) {
        str_concat(&res, "\n");
        r = to_string(node->second, tabs + 2);
        str_concat(&res, r);
        free(r);
    } else {
        str_concat(&res, "NULL");
    }
    str_concat(&res, "\n");
    for (int i = 0; i < tabs + 1; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "condition: ");

    if (node->first != NULL) {
        str_concat(&res, "\n");
        r = to_string(node->first, tabs + 2);
        str_concat(&res, r);
        free(r);
    } else {
        str_concat(&res, "NULL");
    }
    str_concat(&res, "\n");
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "}");
    return res;
}

char* Create_to_string(Node* node, int tabs) {
    char* res = malloc(sizeof(char));
    res[0] = '\0';
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "Create {\n");
    for (int i = 0; i < tabs + 1; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "name: ");
    str_concat(&res, node->v_first.str);
    str_concat(&res, "\n");
    char* r = to_string(node->first, tabs + 1);
    str_concat(&res, r);
    free(r);
    str_concat(&res, "\n");
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "}");
    return res;
}

char* Drop_to_string(Node* node, int tabs) {
    char* res = malloc(sizeof(char));
    res[0] = '\0';
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "Drop { ");
    str_concat(&res, node->v_first.str);
    str_concat(&res, " }");
    return res;
}

char* Where_to_string(Node* node, int tabs) {
    char* res = malloc(sizeof(char));
    res[0] = '\0';
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "Where {\n");
    char* r = to_string(node->first, tabs + 1);
    str_concat(&res, r);
    free(r);
    str_concat(&res, "\n");
    for (int i = 0; i < tabs + 1; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, LogicOperation_strings[node->v_first.log_op]);
    str_concat(&res, "\n");
    r = to_string(node->second, tabs + 1);
    str_concat(&res, r);
    free(r);
    str_concat(&res, "\n");
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "}");
    return res;
}

char* Compare_to_string(Node* node, int tabs) {
    char* res = malloc(sizeof(char));
    res[0] = '\0';
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "Compare {\n");
    char* r = to_string(node->first, tabs + 1);
    str_concat(&res, r);
    free(r);
    str_concat(&res, "\n");
    for (int i = 0; i < tabs + 1; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, Comparison_strings[node->v_first.comp]);
    str_concat(&res, "\n");
    r = to_string(node->second, tabs + 1);
    str_concat(&res, r);
    free(r);
    str_concat(&res, "\n");
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "}");
    return res;
}

typedef char*(*to_string_func)(Node*, int);

to_string_func to_string_functions[] = {
    Column_to_string,
    String_to_string,
    Integer_to_string,
    Float_to_string,
    Bool_to_string,
    List_to_string,
    Pair_to_string,
    Select_to_string,
    Insert_to_string,
    Delete_to_string,
    Update_to_string,
    Create_to_string,
    Drop_to_string,
    Where_to_string,
    Compare_to_string,
    Type_to_string
};

char* to_string(Node* node, int tabs) {
    return to_string_functions[node->type](node, tabs);
}
